autodep8 (0.12) UNRELEASED; urgency=medium

  [ Iain Lane ]
  * nodejs: We don't need to do fancy quoting any more. Since autopkgtest
    commit 702c31af, autopkgtest will escape ' properly for us.

  [ Ondřej Nový ]
  * Use AUTOPKGTEST_TMP instead of ADTTMP

 -- Iain Lane <iain.lane@canonical.com>  Mon, 26 Mar 2018 12:57:52 +0100

autodep8 (0.11.1) unstable; urgency=medium

  * Makefile: install manpage only once, fixing frequent (but not
    deterministic) failures in parallel builds.

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 02 Feb 2018 11:02:23 -0200

autodep8 (0.11) unstable; urgency=medium

  * Team upload.

  * Use dh-octave-autopkgtest for Octave support
  * Use debhelper compatibility level 11
  * d/control: Bump Standards-Version to 4.1.3 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 01 Feb 2018 19:12:32 -0200

autodep8 (0.10) unstable; urgency=medium

  * Team upload.

  [ Antonio Terceiro ]
  * Mention the Go support in the package description

  [ Rafael Laboissiere ]
  * Add support for Octave-Forge packages (Closes: #874306)
  * Bump Standards-Version to 4.1.0 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Tue, 12 Sep 2017 11:38:05 -0300

autodep8 (0.9) unstable; urgency=medium

  [ Martín Ferrari ]
  * Add support for Go package testsuites run by dh_golang_autopkgtest.

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 26 May 2017 07:42:20 -0300

autodep8 (0.8) unstable; urgency=medium

  [ Sean Whitton ]
  * Add support for ELPA package testsuites run by dh_elpa_test
    (Closes: #823979).
  * Add test suite for ELPA package testsuite detection.
  * Bump copyright years in d/copyright.

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 25 Aug 2016 20:36:02 -0300

autodep8 (0.7.1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Python: Ignore warnings (stderr) from pyversions

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 01 Aug 2016 14:52:16 -0300

autodep8 (0.7) unstable; urgency=medium

  [ Ondřej Nový ]
  * Python: Run import test on all Python versions

  [ Stefano Rivera ]
  * Ruby: strip build profile restrictions when producing test dependencies
    from build dependencies (Closes: #832567)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 28 Jul 2016 08:57:42 -0300

autodep8 (0.6) unstable; urgency=medium

  [ Ondřej Nový ]
  * Python: Substitute hyphen with underscore for module name

  [ Barry Warsaw ]
  * Python: Added support for PyPy (Closes: #823883)

  [ Antonio Terceiro ]
  * If `debian/tests/control.autodep8` exists, autodep8 will prepend the
    contents of that file to its own output. (Closes: #823931)
  * dkms: add missing `@` in Depends: line of generated test control files
  * manpage: include one example for each supported package type

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 27 May 2016 09:38:30 -0300

autodep8 (0.5.1) unstable; urgency=medium

  * test/helper.sh: include extra information on failed tests to help
    debugging.
  * avoid partial matches in `Testsuite` field, e.g. `r` should never match
    `ruby`
  * make sure supported languages are always tested in a consistent order

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 25 Apr 2016 16:15:02 -0300

autodep8 (0.5) unstable; urgency=medium

  [ Gordon Ball ]
  * Add detection and simple test generation for R
  * Update d/control with supported package types

  [ Antonio Terceiro ]
  * Build and install a manpage out of README.md

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 25 Apr 2016 11:53:47 -0300

autodep8 (0.4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 (no changes needed).
  * Changed Vcs-* URLs to https schema.
  * Fixed list in package description.
  * Added Python support.

  [ Antonio Terceiro ]
  * Add missing NodeJS and DKMS into list of supported package types in this
    package's description
  * NodeJS: change directory to $ADTTMP before trying to require module to
    avoid using code from source package.

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 24 Apr 2016 10:41:33 -0300

autodep8 (0.3) unstable; urgency=medium

  * split tests by language
  * Ruby: remove commented lines from Build-Depends fields
  * NodeJS:
    - also detect NodeJS packages by binary packages
    - add support for reading upstream name from package.json (Closes: #803176)
      - added dependency and build dependency on python3 (autopkgtest itself
        already depends on python3 anyway)
    - add support for reading upstream name from binary package
  * debian/tests/control: added DEP-8 tests to fix a huge incoherence, which
    is autodep8 not being tested itself on CI.

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 26 Nov 2015 17:30:16 -0200

autodep8 (0.2) unstable; urgency=medium

  [ Antonio Terceiro ]
  * debian/control: add Vcs-* fields
  * support/ruby: pass --check-dependencies to gem2deb-test-runner

  [ Martin Pitt ]
  * Switch maintainer to Autopkgtest team, and add myself as uploader, with
    Antonio's consent.
  * Bump Standards-Version to 3.9.6 (no changes necessary).

  [ Jean-Baptiste Lallement ]
  * support/dkms: Add support for dkms packages (Closes: #766668) This needs a
    corresponding "dkms-autopkgtest" script in DKMS itself, see #769095.

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 24 Jul 2015 09:30:40 -0300

autodep8 (0.1) unstable; urgency=medium

  * Initial release.

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 20 Sep 2014 10:45:21 -0300
